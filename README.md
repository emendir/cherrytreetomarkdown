# CherryTree to Markdown converter

## Background

During my pen-200 course, I realized that after taking 450 pages of notes using CherryTree that the export features sucked. A process of manually converting my notes to markdown or word file would have sucked even more.

As a programmer, I decided to quickly hack an MVP version of markdown exporter utilizing the XML format of CherryTree.

I share the source code of the exporter in hope that it can help fellow OSCP-students.

## Dockerized version

Simpler way of installing and using this tool. Dockerfile and readme from https://github.com/ret2src/CherryTreeToMarkdown-Docker/tree/main

### Installation

~~~
$ docker build -t cherry2md .
~~~

### Usage

~~~ bash
$ ls -alh input/
total 272K
drwxr-xr-x 2 user user 4.0K Jul 25 14:58 .
drwxr-xr-x 4 user user 4.0K Jul 25 15:01 ..
-rw-r--r-- 1 user user 264K Jul 25 14:58 MyCherryTreeNotes.ctd

$ ls -alh output/
total 168K
drwxr-xr-x 4 user user 4.0K Jul 25 14:58 .
drwxr-xr-x 4 user user 4.0K Jul 25 15:01 ..

$ docker run --rm -it --name cherry2md -v "${PWD}/input:/root/cherrytreetomarkdown/volumes/input" -v "${PWD}/output:/root/cherrytreetomarkdown/volumes/output" cherry2md

root@76eaa5863875:~/cherrytreetomarkdown# php cherrytomd.php ./volumes/input/MyCherryTreeNotes.ctd ./volumes/output/MyCherryTreeNotes/

$ ls -alh output/MyCherryTreeNotes/
total 168K
drwxr-xr-x 4 root root 4.0K Jul 25 15:11 .
drwxr-xr-x 5 user user 4.0K Jul 25 15:11 ..
drwxr-xr-x 2 root root 4.0K Jul 25 15:11 files
drwxr-xr-x 2 root root 4.0K Jul 25 15:11 images
-rw-r--r-- 1 root root 151K Jul 25 15:11 index.md
~~~

**Please Note:** Always make sure to define a new subdirectory for your output, otherwise you'll run into errors like:

> PHP Warning:  mkdir(): File exists in /root/cherrytreetomarkdown/src/logic/CherryToMD.php on line 24

## Usage 

```
usage: cherrytomd.php [<options>] [<args>]

Convert unencrypted cherrytree xml file to markdown

OPTIONS
--anchors, -a       Print anchors and anchor links to markdown as html tags
--help, -?          Display this help.
--hmtmlformat, -f   format text using html tags
--title, -t         print node titles

ARGUMENTS
file        CherryTree file (.ctd)
outputDir   Output directory
```

